$(function(){
    $("[data-bs-toggle='tooltip']").tooltip();
    $("[data-bs-toggle='popover']").popover();
    $('.carousel').carousel({
      interval:3500
    });

    $('#contacto').on('show.bs.modal', function(e){
      console.log('el modal se esta mostrando');

      $('#contactoBtn').removeClass('btn-outline-success'); //cambia el color
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled',true);
    });

    $('#contacto').on('shown.bs.modal', function(e){
      console.log('el modal terminó de mostrarse');
    });

    $('#contacto').on('hide.bs.modal', function(e){
      console.log('el modal se está ocultando');
    });

    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('el modal terminó de ocultarse');
      $('#contactoBtn').prop('disabled',false);
    });

    //var myModalEl = document.getElementById('myModal')
    //myModalEl.addEventListener('hidden.bs.modal', function (event) {
    // do something...
    //})
  })